package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import java.util.Set;

public interface PostService {

    //create a post
    void createPost(String stringToken, Post post);

    //getting all posts
    Iterable<Post> getPosts();

    //Edit a post method
    ResponseEntity updatePost(Long id, String stringToken, Post post );

    //delete a use post
    ResponseEntity deletePost(Long id, String stringToken);


    //get users post
    Set<Post> getUserPosts(String stringToken);


}
