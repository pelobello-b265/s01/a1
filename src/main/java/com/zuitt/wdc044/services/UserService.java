package com.zuitt.wdc044.services;
import  com.zuitt.wdc044.models.User;
import java.util.Optional;
//Abstraction
//This will be the interface that will be used to register a user via userController
public interface UserService {
    //Registration of a user
    void createUser(User user);

    //checking if user exists
    // Optional - container object - check if the user exist within the Database; will return null if user does not exist or return the existing user
    Optional<User> findByUsername(String username);

}
