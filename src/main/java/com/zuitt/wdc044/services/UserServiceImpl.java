package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
//add service annotation
//used to indicate that this class will have our function/ business logic of our app/service
public class UserServiceImpl implements UserService{

    @Autowired
    //Autowired use to access object and methods of another class
    private UserRepository userRepository;

    //create a user
    public void createUser(User user){
        userRepository.save(user);
        //save or store user in db
    }

    //Check if user exists
        //return an entity based on the given criteria
        // an empty instance of the optional class
    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

}
