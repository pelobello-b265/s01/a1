package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.graphql.ConditionalOnGraphQlSchema;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//"@SpringBootApplication" this is called as "Annotations" mark.
	// Annotations are used to provide supplemental information about the program
	// There are used to manage and configure the behavior of the framework
	// use for additional used for the method or describe the methods in our file or use to manage and configure our application
@SpringBootApplication

@RestController
// Tells the spring boot that this will handle endpoints for the web request.
//yung mga services ay naka map sa corresponding http and methods
public class Wdc044Application {

	public static void main(String[] args) {
		// This method starts the whole Spring framework
		SpringApplication.run(Wdc044Application.class, args);
		//pram - lahat ng classes na nirurun within the package ay magiging accesible at maeexcute sya
	}
	//This is used for mapping HTTP GET request
	// "@RequestParam" is used to extract query parameters, form parameters and even files from th request
	//name = john; Hello John
	// To append the URL with a name parameter we do the following
	//http://localhost:8080/hello?name=John
	//"?" means the start of the parameter


	@GetMapping("/hello")//end point
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
	}
	@GetMapping("/greeting")
	public String greetings (@RequestParam(value = "greet", defaultValue = "world") String greet){
		return String.format("Good evening, %s! Welcome to zuitt", greet);
	}
	@GetMapping("/contactInfo")
	public String contactInfo (@RequestParam(value = "name", defaultValue = "User") String name, @RequestParam(value = "email", defaultValue = "user@mail.com") String email){
		return String.format("Hello %s! Your email is %s", name, email);
	}

	@GetMapping("/hi")
	public String hi (@RequestParam(value = "name", defaultValue = "User") String name){
		return String.format("Hi %s!", name);
	}

	@GetMapping("/nameAge")
	public String nameAge (@RequestParam(value = "name", defaultValue = "User") String name ,@RequestParam(value = "age", defaultValue = "20") Integer age ){
		return String.format("Hi %s! Your age is %d", name, age);
	}



}
