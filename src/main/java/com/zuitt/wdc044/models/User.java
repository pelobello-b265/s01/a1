package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue
    private  Long id;

    @Column
    private String username;

    @Column
    private  String password;

    //represents the one side of the relationship
    @OneToMany(mappedBy = "user")
    //to prevent the infinite recursion
    @JsonIgnore
    private Set<Post> posts;


    public User(){
        //default constructor
    }

    public User(String username, String password){
        //constructor with argument
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    // Getter and Setter for userName
    public  String getUsername() {
        return username;
    }
    public void setUserName(String username) {
        this.username = username;
    }

    // Getter and Setter for password
    public String getPassword() {
        return  password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Post> getPosts(){
        return posts;
    }
}
