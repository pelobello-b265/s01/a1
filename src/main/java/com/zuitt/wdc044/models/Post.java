package com.zuitt.wdc044.models;

import javax.persistence.*;

// @Entity - Annotation  post class dbtable
//mark this Java object as a representation of a database table via @Entity
@Entity
//designate table name via @Table
@Table(name="posts")

public class Post {

    @Id
    //indicate that this property represents the primary key via @Id
    @GeneratedValue
    //Values for this property will be auto incremented
    private  Long id;

    @Column
    //indicates that the title is a column in the db able
    private String title;

    @Column
    //indicates that the content is a column in the db able
    private  String content;

    //represents the many side of the relationship
    @ManyToOne
    //to reference the foreign key column
    @JoinColumn(name = "user_id", nullable = false)
    private User user;


    public Post(){
        //default constructor
    }

    public Post(String title, String content){
        //constructor with argument
        this.title = title;
        this.content = content;
    }

    // Getter and Setter for title
    public  String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    // Getter and Setter for content
    public String getContent() {
        return  content;
    }

    public void setContent(String content) {

        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }



}
