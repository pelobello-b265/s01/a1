package com.zuitt.wdc044.models;

//mag hahandle ng response ni JWT
//JwtResponse create model for token response
//actual token response pag nag generate na ng token

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;

    private final String jwttoken;

    public JwtResponse(String jwttoken){
        this.jwttoken = jwttoken;
    }

    public String getToken(){
        return this.jwttoken;
    }

}
