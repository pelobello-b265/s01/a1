package com.zuitt.wdc044.repositories;

import  com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
//Contains the methods of the CRUD
//class that has the behavior of the Crud
public interface PostRepository extends CrudRepository<Post, Object> {
    //extends CrudRepository<Post, Object>
    //may build-in na methods
    //contain CRUD functionalities




}
